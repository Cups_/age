﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using AGE.GPU;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Security;
using System.Text;
using System.Windows.Forms;

using static AGE.UTILS.Utils;
using static AGE.Shared;

namespace AGE
{
    public partial class Form1 : Form
    {

        public Form1()
        {

            InitializeComponent();
        }

        private void loadCartridgeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenCartridgeFileDialog = new OpenFileDialog()
            {
                FileName = "",
                Filter = "Gameboy ROM File|*.gb",
                Title = "AGE - Load ROM File",
                Multiselect = false,
                CheckFileExists = true
            };

            if (OpenCartridgeFileDialog.ShowDialog() == DialogResult.OK)
            {
                Gameboy.InsertCartridge(OpenCartridgeFileDialog.FileName);

                this.Text = "AGE - " + Gameboy.Cartridge.Title;
            }

        }

        private void togglePowerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Gameboy.TurnGameBoyOn();
            this.Text = "AGE - " + Gameboy.Cartridge.Title + "(Running)"; 
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!object.Equals(Gameboy.Cpu, null) || !object.Equals(Gameboy.Cpu.CpuThread, null))
                Gameboy.Cpu.CpuThread.Abort();

            // We wait until the CPU thread is done
            while (Gameboy.Cpu.CpuThread.IsAlive)
            {
                // This is to prevent CPU intensive usage.
                System.Threading.Thread.Sleep(10);   
            }
        }

        private void debugButtonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LcdController lcdController = new LcdController(ScreenPanel);
            
        }
    }
}
