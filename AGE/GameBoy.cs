﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using AGE.Cartridge;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AGE.CPU;
using AGE.Map;

using static AGE.Shared;
using AGE.GPU;
using AGE.SPU;
using AGE.IO;

namespace AGE
{
    public class GameBoy
    {
        /// <summary>
        /// The Gameboy cartridge
        /// </summary>
        public GameBoyCartridge Cartridge;

        /// <summary>
        /// The Gameboy CPU
        /// </summary>
        public GameBoyCPU Cpu;
        
        /// <summary>
        /// The Gameboy GPU
        /// </summary>
        public GameBoyGPU Gpu;

        /// <summary>
        /// The Gameboy SPU
        /// </summary>
        public GameBoySPU Spu; 

        /// <summary>
        /// The Gameboy memory ma.. do you really need these comments anyway.
        /// </summary>
        public GameBoyMap MemoryMap = null;

        /// <summary>
        /// The 8kB internal RAM of the gameboy.
        /// </summary>
        public byte[] InternalRAM = null;

        /// <summary>
        /// The ZeroPage RAM of the gameboy.
        /// </summary>
        public byte[] ZeroPageRAM = null;

        /// <summary>
        /// Gameboy timing management object.
        /// </summary>
        public Timers Timer;


        public bool IsCartridgeIn()
        {
            return (Cartridge != null);
        }

        public GameBoy()
        {
            
        }

        public void Tick()
        {
            Gameboy.Timer.Tick();
            Gameboy.Gpu.Tick();
        }

        public void TurnGameBoyOn()
        {
            

            // The Gameboy memory map
            this.MemoryMap = new GameBoyMap();

            // The Gameboy CPU
            this.Cpu = new GameBoyCPU();

            this.InternalRAM = new byte[8192];

            this.ZeroPageRAM = new byte[GameBoyMap.ZPRAM.Item2 - GameBoyMap.ZPRAM.Item1];

            this.Gpu = new GameBoyGPU();

            this.Spu = new GameBoySPU();

            this.Timer = new Timers();

            // If a cartridge is in, we load it into memory
            if (IsCartridgeIn())
                LoadCartridgeIntoMemory(Cartridge);
        }

        public void RemoveCartridge()
        {
            Cartridge = null;
        }

        public void InsertCartridge(string path)
        {
            using (FileStream fileStream = File.OpenRead(path))
            {        
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                Cartridge = new GameBoyCartridge(memStream);               
            }
        }

        public void LoadCartridgeIntoMemory(GameBoyCartridge cartridge)
        {
            Console.WriteLine("[Info] Loaded cartridge : " + cartridge.Title + ", size : " + cartridge.CartridgeRom.Length);
        }
    }
}
