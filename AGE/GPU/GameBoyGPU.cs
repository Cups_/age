﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGE.GPU
{
    public class BGP
    {
        public byte DataForDot11;
        public byte DataForDot10;
        public byte DataForDot01;
        public byte DataForDot00;

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= DataForDot00;
            returnByte |= (byte)(DataForDot01 << 2);
            returnByte |= (byte)(DataForDot10 << 4);
            returnByte |= (byte)(DataForDot11 << 6);

            return returnByte;
        }

        public static BGP ToBGP(byte b)
        {
            BGP returnBGP = new BGP();

            returnBGP.DataForDot00 = (byte)(b & 3);
            returnBGP.DataForDot01 = (byte)((b & 12) >> 2);
            returnBGP.DataForDot10 = (byte)((b & 48) >> 4);
            returnBGP.DataForDot11 = (byte)((b & 192) >> 6);

            return returnBGP;
        }
    }

    public class LCDC
    {
        public bool OperationEnabled;
        public bool TileMapSelect;
        public bool Display;
        public bool BGWindowTileDataSelect;
        public bool BGTileMapDisplaySelect;
        public bool OBJSpriteSize;
        public bool OBJSpriteDisplay;
        public bool BGAndWindowDisplay;

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= (byte)(Convert.ToByte(BGAndWindowDisplay));
            returnByte |= (byte)(Convert.ToByte(OBJSpriteDisplay) << 1);
            returnByte |= (byte)(Convert.ToByte(OBJSpriteSize) << 2);
            returnByte |= (byte)(Convert.ToByte(BGTileMapDisplaySelect) << 3);
            returnByte |= (byte)(Convert.ToByte(BGWindowTileDataSelect) << 4);
            returnByte |= (byte)(Convert.ToByte(Display) << 5);
            returnByte |= (byte)(Convert.ToByte(TileMapSelect) << 6);
            returnByte |= (byte)(Convert.ToByte(OperationEnabled) << 7);

            return returnByte;
        }

        public static LCDC ToLCDC(byte b)
        {
            LCDC returnLCDC = new LCDC();

            returnLCDC.BGAndWindowDisplay = (b & 1) != 0;
            returnLCDC.OBJSpriteDisplay = (b & 2) != 0;
            returnLCDC.OBJSpriteSize = (b & 4) != 0;
            returnLCDC.BGTileMapDisplaySelect = (b & 8) != 0;
            returnLCDC.BGWindowTileDataSelect = (b & 16) != 0;
            returnLCDC.Display = (b & 32) != 0;
            returnLCDC.TileMapSelect = (b & 64) != 0;
            returnLCDC.OperationEnabled = (b & 128) != 0;

            return returnLCDC;
        }
    }

    public enum ModeFlag
    {
        HBlank,
        VBlank,
        Searching,
        Transfering
    }

    public class GameBoyGPU
    {
        /// <summary>
        /// A complete cycle through these states 
        /// </summary>
        public const int HorizontalLine = 456;

        /// <summary>
        /// 
        /// </summary>
        public const int VerticalLine = 70224 / 456;


        /// <summary>
        /// The sprite attribute memory. 0xFE00 - 0xFE9F.
        /// </summary>
        public byte[] SpriteAttribMemory = null;

        /// <summary>
        /// The video RAM. 0x8000 - 0x9FFF.
        /// </summary>
        public byte[] VRAM = null;

        /// <summary>
        /// LCD Control object
        /// </summary>
        public LCDC LCDControl = null;
        
        public byte LY = 0;

        /// <summary>
        /// The size of the screen in pixels.
        /// </summary>
        public Size ScreenSize = new Size(160, 144);

        public bool InterruptVBlank = false;

        public int ScrollY = 0;

        public BGP BGP = null;

        public int HorizontalTick = 0;

        public int Line = 0;

        public ModeFlag Mode = ModeFlag.Searching;

        public GameBoyGPU()
        {
            SpriteAttribMemory = new byte[160];
            VRAM = new byte[Map.GameBoyMap.VRAM.Item2 - Map.GameBoyMap.VRAM.Item1 + 1];

            // This is for shit and giggles, the ram on startup is filled with random values so we do it too.
            Random r = new Random();
            for (int i = 0; i < VRAM.Length; i++)
            {
                VRAM[i] = (byte)r.Next(0, 256);
            }

            BGP = new BGP();
        }

        /// <summary>
        /// Renders and draws the pixel at the coords specified.
        /// </summary>
        /// <param name="point"></param>
        public void RenderPixel(Point point)
        {
            
        }

        public void Tick()
        {
            // We ticked, so we increase the horizontal tick amount and mod it to the total amount of hor ticks.
            HorizontalTick = (HorizontalTick + 1) % HorizontalLine;

            switch(Mode)
            {
                case ModeFlag.VBlank:
                    // We are in a VBlank, we need to increase the VBlank tick amount if we are in the beginning of a new line.
                    if(HorizontalTick == 0)
                    {
                        Line = (Line + 1) % VerticalLine;

                        Mode = (Line == 0) ? ModeFlag.Searching : Mode;
                    }
                    break;
                default:
                    switch (HorizontalTick)
                    {
                        case 0:
                            // We finished drawing a line xd
                            Line++;

                            // We reached the end of the screen. We need to trigger the VBlank interrupt.
                            if(Line >= ScreenSize.Height)
                            {
                                InterruptVBlank = true;
                            }
                            else
                            {
                                // Else, we need to ready for the next line.
                                Mode = ModeFlag.Searching; 
                            }
                            break;
                        case 252: 
                            // We reached the end of the line. We need to set the HBlank.
                            Mode = ModeFlag.HBlank;
                            break;
                        case 80: // Page 53 (77 - 83), I averaged to 80, this corresponds to Mode 10 (2)
                            Mode = ModeFlag.Transfering; // The CPU cannot access the OAM during this period
                            break;
                    }
                    break;
            }

            if (Mode != ModeFlag.VBlank)
            {
                for (int x = 0; x <= ScreenSize.Width; x++)
                {
                    RenderPixel(new Point(x, Line));
                }
            }
        }
    }
}
