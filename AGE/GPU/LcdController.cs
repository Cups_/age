﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AGE.GPU
{
    /// <summary>
    /// This class is useless right now, i need to find a good 2D drawing library for C#
    /// </summary>
    public class LcdController
    {
        /// <summary>
        /// Represents every pixel color supported by gameboy
        /// </summary>
        public enum PixelColor
        {
            Off = 0,
            Low = 1,
            High = 2,
            On = 3
        }

        /// <summary>
        /// Get pixel color from enum
        /// </summary>
        /// <param name="pc">The pixel color enum</param>
        /// <returns></returns>
        public static Color GetPixelColor(PixelColor pc)
        {
            switch (pc)
            {
                case PixelColor.Off:
                    return Color.FromArgb(255, 255, 255);
                case PixelColor.Low:
                    return Color.FromArgb(192, 192, 192);
                case PixelColor.High:
                    return Color.FromArgb(96, 96, 96);
                case PixelColor.On:
                    return Color.FromArgb(0, 0, 0);
                default:
                    return Color.FromArgb(255, 255, 255);
            }
        }

        public const int LCD_WIDTH = 160;
        public const int LCD_HEIGHT = 144;

        public int fps = 60;

        public Panel LcdPanel = null;
        public Thread GpuThread = null;
        public Graphics LcdGraphics = null;

        public LcdController(Panel lcdPanel)
        {
            LcdPanel = lcdPanel;
            LcdGraphics = LcdPanel.CreateGraphics();
            LcdPanel.Paint += LcdPanel_Paint;
            Control.CheckForIllegalCrossThreadCalls = false;
            GpuThread = new Thread(() => GraphicsLoop());
            GpuThread.Name = "GPU Thread";
            GpuThread.Priority = ThreadPriority.Highest;
            GpuThread.Start();
        }


        private void GraphicsLoop()
        {
            int heightRatio = LcdPanel.Size.Height / LCD_HEIGHT;
            int widthRatio = LcdPanel.Size.Width / LCD_WIDTH;
            Random r = new Random();

            DateTime d = DateTime.Now;
            
            for (int width = 0; width < LcdController.LCD_WIDTH * 4; width += 4)
            {
                for (int height = 0; height < LcdController.LCD_HEIGHT * 4; height += 4)
                {
                    
                    Brush drawPen = new SolidBrush(GetPixelColor((LcdController.PixelColor)r.Next(3)));
                    LcdGraphics.FillRectangle(drawPen, new Rectangle(width, height, widthRatio, heightRatio));
                }
            }
            Console.WriteLine("Drew in " + (DateTime.Now - d).TotalMilliseconds);
            LcdGraphics.DrawString("Drew in : " + (DateTime.Now - d).TotalMilliseconds, new Font("Arial", 50), new SolidBrush(Color.Black), new PointF(10, 10));



        }

        
        
        private void LcdPanel_Paint(object sender, PaintEventArgs e)
        {
            
        }

        public void DrawToScreen(Point pixelLocation, PixelColor pc) 
        {
            // We get the pixel ratio based on panel size
            
        }
    }
}
