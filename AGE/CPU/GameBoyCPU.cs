﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using static AGE.Shared;

namespace AGE.CPU
{
    public class GameBoyCPU
    {
        
        /// <summary>
        /// We set this flag if the opcodes == 0xCB as the next instruction will be in another instruction set
        /// </summary>
        public bool Got0xCB = false;
       
        public byte[] Bios = new byte[256]
        {
            0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32,
            0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E,
            0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3,
            0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0,
            0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A,
            0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B,
            0xFE, 0x34, 0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06,
            0x08, 0x1A, 0x13, 0x22, 0x23, 0x05, 0x20, 0xF9,
            0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21, 0x2F, 0x99,
            0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
            0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64,
            0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04,
            0x1E, 0x02, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90,
            0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2,
            0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62,
            0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06,
            0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xF2, 0xF0, 0x42,
            0x90, 0xE0, 0x42, 0x15, 0x20, 0xD2, 0x05, 0x20,
            0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F, 0x06, 0x04,
            0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
            0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9,
            0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
            0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
            0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
            0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
            0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
            0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E,
            0x3C, 0x42, 0xB9, 0xA5, 0xB9, 0xA5, 0x42, 0x4C,
            0x21, 0x04, 0x01, 0x11, 0xA8, 0x00, 0x1A, 0x13,
            0xBE, 0x20, 0xFE, 0x23, 0x7D, 0xFE, 0x34, 0x20,
            0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20,
            0xFB, 0x86, 0x20, 0xFE, 0x3E, 0x01, 0xE0, 0x50
        };

        /// <summary>
        /// The main cpu thread
        /// </summary>
        public Thread CpuThread;

        /// <summary>
        /// The IME (interrupt master enable) flag is reset by DI and prohibits all interrupts (when set to false)
        /// </summary>
        public bool InterruptMasterEnable = true;

        /// <summary>
        /// We need to run an instruction before interrupting so we need this flag xd
        /// </summary>
        public bool InterruptMasterEnableNext = true;

        /// <summary>
        /// Number of instructions ran by burst.
        /// </summary>
        public const int GRANULARITY = 0x10000;

        /// <summary>
        /// The frequency in Hz of the system clock.
        /// </summary>
        public const int FREQUENCY = (int)4.194304 * 1_000_000;

        /// <summary>
        /// This flag is set on a tick, and is unset immediately after. TODO: Check if this is good enough.
        /// </summary>
        public static bool TickFlag = false;

        /// <summary>
        /// This thread objective is to tick, that's it.
        /// </summary>
        public Thread TickThread = null;

        /// <summary>
        /// The burst duration in nanoseconds
        /// </summary>
        public long BurstDuration = GRANULARITY * (1_000_000_000 / FREQUENCY);

        /// <summary>
        /// The actual interrupt flag, if set to true and the IME aswell, the CPU will interrupt
        /// </summary>

        public bool InterruptFlag = false;

        #region REGISTERS
        public struct RegistersFlag
        {
            /// <summary>
            /// Zero Flag, represents result of a compare math equation .
            /// </summary>
            public bool Z { get; set; }

            /// <summary>
            /// Substract Flag, set if last math op was a sub.
            /// </summary>
            public bool N { get; set; }

            /// <summary>
            /// Half Carry flag, This bit is set if a carry occurred from the lower demi octet in the last math operation
            /// </summary>
            public bool H { get; set; }

            /// <summary>
            /// Carry Flag, This bit is set if a carry occurred from the last math operation or if register A is the smaller value when executing the CP instruction
            /// </summary>
            public bool C { get; set; }
        }

        public struct RegistersStruct
        {
            public ushort ProgramCounter;
            public ushort StackPointer;

            public byte A;
            public byte B;
            public byte C;
            public byte D;
            public byte E;
            public byte H;
            public byte L;

            public RegistersFlag F;
        }
        #endregion

        public RegistersStruct Registers;

        /// <summary>
        /// See Low power mode in specification, page 19.
        /// </summary>
        public bool Halted = false;

        public byte InstructionCycles { get; set; }

        /// <summary>
        /// Return a byte representation of the F registers TODO : Check it 
        /// </summary>
        /// <returns></returns>
        public byte GetFValue()
        {
            byte returnValue = 0b0000_0000;
            returnValue = (byte)((Convert.ToByte(Registers.F.Z) << 7) | (Convert.ToByte(Registers.F.N) << 6) | (Convert.ToByte(Registers.F.H) << 5) | (Convert.ToByte(Registers.F.C) << 4));
            return returnValue;
        }

        /// <summary>
        /// Get USHORT from two registers.
        /// </summary>
        /// <param name="r1">High Endian Byte</param>
        /// <param name="r2">Low Endian Byte</param>
        /// <returns></returns>
        public ushort GetUshortFromRegisters(byte r1, byte r2)
        {
            return (ushort)(r2 | (r1 << 8));
        }

        /// <summary>
        /// Set two registers to an USHORT.
        /// </summary>
        /// <param name="r1">High Endian Byte</param>
        /// <param name="r2">Low Endian Byte</param>
        /// <param name="value"></param>
        public void SetCombinedRegisters(ref byte r1, ref byte r2, ushort value)
        {
            r1 = (byte)(value >> 8);
            r2 = (byte)(value);
        }

        public GameBoyCPU()
        {
            // Init Registers
            Registers = new RegistersStruct
            {
                A = 0,
                B = 0,
                C = 0,
                D = 0,
                E = 0,
                F = new RegistersFlag(),
                H = 0,
                L = 0,
                ProgramCounter = 0,
                StackPointer = 0
            };
            // 

            // Load Bios
            MountBios();

            TickThread = new Thread(() =>
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    Thread.Sleep((int)BurstDuration / 1_000_000);
                    TickFlag = !TickFlag;
                }
                
            })
            {
                Name = "Tick Thread",
                Priority = ThreadPriority.Highest
            };
            TickThread.Start();

            CpuThread = new Thread(() =>
            {
                CpuLoop();
            })
            {
                Name = "LR35902 Thread",
                Priority = ThreadPriority.AboveNormal
            };
            CpuThread.Start();


        }

        public void StoreByte(ref byte r1, byte value)
        {
            r1 = value;
        }

        public void PushByte(byte b)
        {
            Registers.StackPointer--;
            Gameboy.MemoryMap.WriteToMemory(Registers.StackPointer, b);
        }

        public void PushWord(ushort w)
        {
            PushByte((byte)(w >> 8));
            PushByte((byte)w);
        }

        public byte PopByte()
        {
            return Gameboy.MemoryMap.ReadFromMemory(Registers.StackPointer++);
        }

        public ushort PopWord()
        {
            return (ushort)(PopByte() | (PopByte() << 8));
        }

        /// <summary>
        /// Clear bios mounted at 0x0
        /// </summary>
        public void UnmountBios()
        {
            for (ushort i = 0; i < Bios.Length; i++)
            {
                Gameboy.MemoryMap.WriteToMemory((ushort)(0x0 + i), 0x0);
            }

            Gameboy.MemoryMap.BootRomMounted = false;
        }

        /// <summary>
        /// Load BIOS starting at 0x0
        /// </summary>
        public void MountBios()
        {
            for (int i = 0; i < Bios.Length; i++)
            {
                Gameboy.MemoryMap.WriteToMemory((ushort)(0x0 + i), Bios[i]);
            }

            Gameboy.MemoryMap.BootRomMounted = true;

            Console.WriteLine("[Info] Mounted bios at 0x0, length : " + Bios.Length);           
        }

        public static string OpCodesToImplement = "";

        public int SpentCycles = 0;

        public void WaitForTick()
        {
            bool oldValue = TickFlag;
            while (TickFlag == oldValue) { }
        }

        public void CpuLoop()
        {
            while (true)
            {
                while(SpentCycles < GRANULARITY)
                {
                    // We get the OPCODE
                    byte opCode = Gameboy.MemoryMap.ReadFromMemory(Registers.ProgramCounter);

                    try
                    {
                        if (Halted)
                        {
                            SpentCycles++;

                            Gameboy.Tick();

                            // TODO : CHECK THIS
                            if (InterruptFlag)
                            {
                                Halted = false;
                            }
                            else
                            {
                                return;
                            }
                        }

                        Registers.ProgramCounter++;

                        if (opCode == 0xCB)
                        {
                            // We set the Got0xCB flag to tell the cpu we need to change instruction set
                            Got0xCB = true;

                            // And get the next opcode
                            opCode = Gameboy.MemoryMap.ReadFromMemory(Registers.ProgramCounter);

                            // We read the next opcode, we can increment the PC again.
                            Registers.ProgramCounter++;
                        }

                        // We retrieve the next instruction on the belonging instruction set (based on Got0xCB flag)
                        Instruction inst = (Got0xCB ? (Instructions.CBInstructionSet[opCode]) : (Instructions.InstructionSet[opCode]));

                        // We call the instruction "action".
                        inst.action.DynamicInvoke();

                        SpentCycles += inst.duration;

                        // Console.WriteLine("INST : " + (Got0xCB ? "0xCB" + opCode.ToString("X") : "0x" + opCode.ToString("X")) + " | PC : " + Registers.ProgramCounter.ToString() + " | SP : " + Registers.StackPointer + " | B : " + Registers.B + " | Z : " + Registers.F.Z);

                        // The only Opcode to have a delay of 1 cycle is NOP so in order to not mess up anything I'd rather log if otherwise.
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            if (opCode != 0x0 && inst.duration == 1)
                                Console.WriteLine(String.Format("OPCode #{0} has an invalid duration of 1 cycle.", opCode.ToString("X")));
                        }

                        // Instructions on 0xCB are 16 bits wide so the next instruction shouldn't be marked as 0xCB so we reset the flag
                        if (Got0xCB) Got0xCB = false;

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(Got0xCB ? "Please Implement : 0xCB" + opCode.ToString("X") : "Please Implement : 0x" + opCode.ToString("X"));
                        OpCodesToImplement += Got0xCB ? "Please Implement : 0xCB" + opCode.ToString("X") : "Please Implement : 0x" + opCode.ToString("X") + Environment.NewLine;
                        Console.WriteLine(ex.Message);
                    }
                }

                // We fired a burst, now we need to wait for the next tick.
                SpentCycles -= GRANULARITY;

                WaitForTick();

                // TODO CHeck
                Gameboy.Tick();
            }
        }
    }
}
