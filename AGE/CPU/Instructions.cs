﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WAR TY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGE.CPU;

using AGE.UTILS;
using static AGE.Shared;

namespace AGE.CPU
{
    public class Instructions
    {
        
        // TODO MAKE A HUGE FUCKING CHECK OF ALL IMPLEMENTATIONS BEFORE U ADD MORE OK THX
        private static void nop()
        {
            
        }

        /// <summary>
        /// Returns the byte read from PC address and then increments it 
        /// </summary>
        /// <returns></returns>
        private static byte GetFollowingByte()
        {
            return Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.Registers.ProgramCounter++);
        }

        /// <summary>
        /// Returns the word read from PC address and then increments it 
        /// </summary>
        /// <returns></returns>
        private static ushort GetFollowingWord()
        {
            byte b1 = GetFollowingByte();
            byte b2 = GetFollowingByte();
         
            return (ushort)(b1 | (b2 << 8));
        }

        /// <summary>
        /// Jump, aka sets the program counter to the next following word
        /// </summary>
        private static void jp()
        {
            Gameboy.Cpu.Registers.ProgramCounter = GetFollowingWord();
        }

        /// <summary>
        /// Performs a xor operation and set flags accordingl.
        /// </summary>
        /// <param name="r1"></param>
        private static void xor(byte r1)
        {
            Gameboy.Cpu.Registers.A = (byte)(r1 ^ Gameboy.Cpu.Registers.A);

            Gameboy.Cpu.Registers.F.Z = Gameboy.Cpu.Registers.A == 0x0;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.H = false;
            Gameboy.Cpu.Registers.F.C = false;
        }

        private static void ld_n_nn(ref byte r1, ref byte r2)
        {
            r2 = GetFollowingByte();
            r1 = GetFollowingByte();           
        }

        private static void ld_hl_a()
        {
            Gameboy.Cpu.SetCombinedRegisters(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L, (((ushort)Gameboy.Cpu.Registers.A)));
        }

        /*
         
        private static void ldd(ref byte r1)
        {
            r1 = Gameboy.MemoryMap.ReadFromMemory(UTILS.Utils.UshortFromByte(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L));
            Gameboy.Cpu.SetCombinedRegisters(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L, (ushort)(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L) - 1));
        }*/

        private static void ldd_mem(ushort memaddress, byte r1)
        {
            Gameboy.MemoryMap.WriteToMemory(memaddress, r1);
    
            dec(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L);    
        }

        private static void ld(ref ushort r1, ushort value)
        {
            r1 = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="r1">High Endian Byte</param>
        /// <param name="r2">Low Endian Byte</param>
        /// <param name="value"></param>
        private static void ld(ref byte r1, ref byte r2, ushort value)
        {
            byte[] bs = Utils.BytesFromUShort(value);

            r1 = bs[1];
            r2 = bs[0];
        }

        // TODO Check get and set value of hl
        private static void ld_sp_hl()
        {
            Gameboy.Cpu.Registers.StackPointer = Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L);
        }
        
        private static void bit(byte bitIndex, byte r1Value)
        {
         
            // We right shift the bit at the corresponding bitIndex and AND it to 1 to see if it is equal to 0.
            Gameboy.Cpu.Registers.F.Z = ((r1Value  & (1 << bitIndex)) == 0);

            // This is according to documentation
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.H = true;
        }

        

        private static void jr(bool cond, sbyte shift)
        {
            // TODO Check this one lmao
            if (cond)
            {   
                unsafe
                {
                    Gameboy.Cpu.Registers.ProgramCounter = (ushort)(shift + Gameboy.Cpu.Registers.ProgramCounter);
                }
            }
        }

        /// <summary>
        /// Put data to memory.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="value"></param>
        private static void ld_address_r(ushort address, byte value)
        {
            Gameboy.MemoryMap.WriteToMemory(address, value);
        }

        /// <summary>
        /// Put data to memory.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="value"></param>
        private static void ld_address_r(ushort address, ushort value)
        {
            Gameboy.MemoryMap.WriteToMemory(address, value);
        }

        // TODO Please make a summary of all ASM functions

        private static void ei()
        {
            // TODO Check this implementation
            Gameboy.Cpu.InterruptMasterEnableNext = true;
        }

        #region LOADS
        private static void ld(ref byte r1, byte value)
        {
            r1 = value;
        }

        private static void ld(ref byte r1, ushort value)
        {
            r1 = (byte)value;
        }

        private static void ld_mem(ushort address, byte value)
        {
            Gameboy.MemoryMap.WriteToMemory(address, value);
        }


        #endregion

        /// <summary>
        /// Compare A with n. This is basically an A - n  subtraction instruction but the results are thrown  away.
        /// </summary>
        /// <param name="b">n</param>
        private static void cp(byte b)
        {
            byte result = (byte)(Gameboy.Cpu.Registers.A - b);

            Gameboy.Cpu.Registers.F.Z = result == 0;
            Gameboy.Cpu.Registers.F.N = true;
            Gameboy.Cpu.Registers.F.C = Gameboy.Cpu.Registers.A < b;
            Gameboy.Cpu.Registers.F.H = (result & 0x10) != 0;
        }

        /// <summary>
        /// Substract n + carry flag from A.
        /// </summary>
        /// <param name="r1">n</param>
        private static void sbc(byte r1)
        {
            // TODO Double check this implementation
            uint substractResult = (uint)Gameboy.Cpu.Registers.A - r1 - Convert.ToUInt32(Gameboy.Cpu.Registers.F.C);

            // Set zero flag if result is 0.
            Gameboy.Cpu.Registers.F.Z = ((byte)substractResult) == 0;

            // Set half-carry flag if no borrow from bit 4. IM UNSURE ABOUT THIS. This should mean if result is 0b0001_0000, we have a carry, but is it really how to implement it?
            Gameboy.Cpu.Registers.F.H = ((Gameboy.Cpu.Registers.A ^ r1 ^ substractResult) & 0x10) != 0;

            // Set the carry flag if no borrow at all 0b0001_0000_0000 (so more than 8 bits)
            Gameboy.Cpu.Registers.F.C = ((substractResult & 0x100) != 0);
            
            // The substract flag is set as we performed a sub op.
            Gameboy.Cpu.Registers.F.N = true;

            Gameboy.Cpu.Registers.A = (byte)substractResult;
        }
       
        private static void and(byte b1)
        {
            Gameboy.Cpu.Registers.A &= b1;

            Gameboy.Cpu.Registers.F.Z = Gameboy.Cpu.Registers.A == 0;
            Gameboy.Cpu.Registers.F.C = false;
            Gameboy.Cpu.Registers.F.H = true;
            Gameboy.Cpu.Registers.F.N = false;
        }

        /// <summary>
        /// Add n to A.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="n"></param>
        private static void add(byte n)
        {
            ushort w1 = (ushort)(Gameboy.Cpu.Registers.A + n);

            Gameboy.Cpu.Registers.F.Z = w1 == 0;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.C = (w1 & 0x100) != 0;
            Gameboy.Cpu.Registers.F.H = ((Gameboy.Cpu.Registers.A ^ n ^ w1) & 0x10) != 0;

            Gameboy.Cpu.Registers.A = (byte)w1;
        }

        private static void dec(ref byte r1)
        {
            r1--;
            
            Gameboy.Cpu.Registers.F.Z = r1 == 0;
            Gameboy.Cpu.Registers.F.N = true;
            Gameboy.Cpu.Registers.F.H = ((r1 & 0xF) == 0);
        }

        private static void dec(ref byte r1, ref byte r2)
        {
            ushort comb = Utils.UshortFromByte(r1, r2);

            comb--;

            Gameboy.Cpu.Registers.F.Z = comb == 0;
            Gameboy.Cpu.Registers.F.N = true;   
            // TODO CHECK
            Gameboy.Cpu.Registers.F.H = (comb & 0xF) == 0;

            Gameboy.Cpu.SetCombinedRegisters(ref r1, ref r2, comb);
        }

        /// <summary>
        /// Increments the combined registers
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        private static void inc(ref byte r1, ref byte r2)
        {
            Gameboy.Cpu.SetCombinedRegisters(ref r1, ref r2, (ushort)(Gameboy.Cpu.GetUshortFromRegisters(r1, r2) + 1));

        }

        // TODO CHECK
        private static void inc(ref byte r1)
        {
            r1--;

            Gameboy.Cpu.Registers.F.Z = r1 == 0;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.H = (Gameboy.Cpu.Registers.F.C & 0xF == 0xF);
        }

        
     
        /// <summary>
        /// Push address of next instruction onto stack and then jump to address nn if condition evaluates to true
        /// </summary>
        /// <param name="condition"></param>
        private static void call(bool condition)
        {
            if (condition)
            {
                // The PC is already incremented so we just need to add two because of the two byte of the address.
                Gameboy.Cpu.PushWord((ushort)(Gameboy.Cpu.Registers.ProgramCounter + 2));
                jp();
            }
        }

        /// <summary>
        /// Add + Carry flag to r1
        /// </summary>
        private static void adc(ref byte r1, byte r2)
        {
            // TODO : CHECK
            byte result = (byte)(r1 + r2 + Convert.ToByte(Gameboy.Cpu.Registers.F.C));

            Gameboy.Cpu.Registers.F.H = ((byte)(r1 ^ r2 ^ result) & 0x10) != 0;
            Gameboy.Cpu.Registers.F.C = (byte)(result & 0x100) != 0;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.Z = result == 0;

            r1 = result;
        }

  
        /// <summary>
        /// Returns a binary string representation of n for debug purposes.  
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        static string GetIntBinaryString(byte n)
        {
            char[] b = new char[8];
            int pos = 7;
            int i = 0;

            while (i < 8)
            {
                if ((n & (1 << i)) != 0)
                {
                    b[pos] = '1';
                }
                else
                {
                    b[pos] = '0';
                }
                pos--;
                i++;
            }
            return new string(b);
        }

        #region ROTATES & SHIFTS
        /// <summary>
        /// Rotate n left through Carry flag.
        /// </summary>
        /// <param name="r1">n</param>
        private static void rl(ref byte r1)
        {
            byte oldValue = Convert.ToByte(Gameboy.Cpu.Registers.F.C);

            Gameboy.Cpu.Registers.F.C = r1 >> 7 != 0;

            r1 <<= 1;
            r1 |= oldValue;

            Gameboy.Cpu.Registers.F.Z = r1 == 0;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.H = false;
        }

        /// <summary>
        /// Rotate A left through Carry flag.
        /// </summary>
        private static void rla()
        {
            byte oldValue = Convert.ToByte(Gameboy.Cpu.Registers.F.C);

            Gameboy.Cpu.Registers.F.C = Gameboy.Cpu.Registers.A >> 7 != 0;

            Gameboy.Cpu.Registers.A <<= 1;
            Gameboy.Cpu.Registers.A |= oldValue;

            Gameboy.Cpu.Registers.F.Z = false;
            Gameboy.Cpu.Registers.F.N = false;
            Gameboy.Cpu.Registers.F.H = false;
        }
        #endregion

        /// <summary>
        /// Pop two bytes from stack & jump to that address
        /// </summary>
        private static void ret()
        {
            Gameboy.Cpu.Registers.ProgramCounter = Gameboy.Cpu.PopWord();
        }

        /// <summary>
        /// Pop two bytes off stack into register pair nn'. Increment Stack Pointer (SP) twice.
        /// </summary>
        /// <param name="r1">n - Higher registry, will store second byte.</param>
        /// <param name="r2">n' - Lower registry, will store first byte.</param>
        private static void pop(ref byte r1, ref byte r2)
        {
            r2 = Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.Registers.StackPointer++);
            r1 = Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.Registers.StackPointer++);
        }

        private static void ldi(ushort address, byte r1)
        {
            Gameboy.MemoryMap.WriteToMemory(address, r1);
            Gameboy.Cpu.SetCombinedRegisters(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L, (ushort)(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L) + 1));
        }

        private static void pushByte(byte b)
        {
            // NOTE : We decrement before as stated in the documentation GBCPUMan Page 63 : "The Stack Pointer automatically decrements before it puts something onto the stack"
            Gameboy.Cpu.Registers.StackPointer -= 1;
            Gameboy.MemoryMap.WriteToMemory(Gameboy.Cpu.Registers.StackPointer, b);     
        }

        private static void ldh(byte mem, byte r1)
        {
            Gameboy.MemoryMap.WriteToMemory((ushort)(0xFF00 | mem), r1);
        }


        /// <summary>
        /// Push register pair nn onto stack. Decrement Stack Pointer (SP) twice
        /// </summary>
        /// <param name="r1">n1</param>
        /// <param name="r2">n2</param>
        private static void push(byte r1, byte r2)
        {
            pushByte(r1);
            pushByte(r2);
        }

        public static Dictionary<ushort, Instruction> InstructionSet = new Dictionary<ushort, Instruction>()
        {
            {0x0, new Instruction(4, new Action(() => nop()))},
            {0x1, new Instruction(12, new Action(() => ld_n_nn(ref (Gameboy.Cpu.Registers).B, ref (Gameboy.Cpu.Registers).C)))},
            {0x2, new Instruction(8, new Action(() => throw new Exception("Not implemented")))},
            {0x3, new Instruction(8, new Action(() => throw new Exception("Not implemented")))},
            {0x4, new Instruction(4, new Action(() => inc(ref Gameboy.Cpu.Registers.B)))},
            {0x5, new Instruction(4, new Action(() => dec(ref Gameboy.Cpu.Registers.B)))},
            {0x6, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.B, GetFollowingByte())))},
            {0x7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8, new Instruction(1, new Action(() => ld_address_r(GetFollowingWord(), Gameboy.Cpu.Registers.StackPointer)))},
            {0x9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC, new Instruction(4, new Action(() => inc(ref Gameboy.Cpu.Registers.C)))},
            {0xD, new Instruction(4, new Action(() => dec(ref Gameboy.Cpu.Registers.C)))},
            {0xE, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.C, GetFollowingByte())))},
            {0xF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x10, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x11, new Instruction(12, new Action(() => ld_n_nn(ref (Gameboy.Cpu.Registers).D, ref (Gameboy.Cpu.Registers).E)))},
            {0x12, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x13, new Instruction(1, new Action(() => inc(ref Gameboy.Cpu.Registers.D, ref Gameboy.Cpu.Registers.E)))},
            {0x14, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x15, new Instruction(4, new Action(() => dec(ref Gameboy.Cpu.Registers.D)))},
            {0x16, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.D, GetFollowingByte())))},
            {0x17, new Instruction(4, new Action(() => rla()))},
            {0x18, new Instruction(8, new Action(() => jr(true, (sbyte)GetFollowingByte())))},
            {0x19, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1A, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.D, Gameboy.Cpu.Registers.E)))))},
            {0x1B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1E, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.E, GetFollowingByte())))},
            {0x1F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x20, new Instruction(8, new Action(() => jr(!Gameboy.Cpu.Registers.F.Z, (sbyte)GetFollowingByte())))},
            {0x21, new Instruction(12, new Action(() => ld(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L, GetFollowingWord())))},
            {0x22, new Instruction(8, new Action(() => ldi(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L), Gameboy.Cpu.Registers.A)))},
            {0x23, new Instruction(1, new Action(() => inc(ref Gameboy.Cpu.Registers.H, ref Gameboy.Cpu.Registers.L)))},
            {0x24, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x25, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x26, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.H,GetFollowingByte())))},
            {0x27, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x28, new Instruction(8, new Action(() => jr(Gameboy.Cpu.Registers.F.Z, (sbyte)GetFollowingByte())))},
            {0x29, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2E, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.L, GetFollowingByte())))},
            {0x2F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x30, new Instruction(8, new Action(() => jr(!Gameboy.Cpu.Registers.F.C, (sbyte)GetFollowingByte())))},
            {0x31, new Instruction(12, new Action(() => ld(ref Gameboy.Cpu.Registers.StackPointer, GetFollowingWord())))},
            {0x32, new Instruction(8, new Action(() => ldd_mem(Utils.UshortFromByte(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L), Gameboy.Cpu.Registers.A)))},
            {0x33, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x34, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x35, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x36, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x37, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x38, new Instruction(8, new Action(() => jr(Gameboy.Cpu.Registers.F.C, (sbyte)GetFollowingByte())))},
            {0x39, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3D, new Instruction(4, new Action(() => dec(ref Gameboy.Cpu.Registers.A)))},
            {0x3E, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.A, GetFollowingByte())))},
            {0x3F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x40, new Instruction(1, new Action(() => ld(ref Gameboy.Cpu.Registers.B, Gameboy.Cpu.Registers.B)))},
            {0x41, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x42, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x43, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x44, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x45, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x46, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x47, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.B, Gameboy.Cpu.Registers.A)))},
            {0x48, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x49, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4F, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.C, Gameboy.Cpu.Registers.A)))},
            {0x50, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x51, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x52, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x53, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x54, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x55, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x56, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x57, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.D, Gameboy.Cpu.Registers.A)))},
            {0x58, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x59, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5F, new Instruction(1, new Action(() => ld(ref Gameboy.Cpu.Registers.E, Gameboy.Cpu.Registers.A)))},
            {0x60, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x61, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x62, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x63, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x64, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x65, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x66, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x67, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.A)))},
            {0x68, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x69, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6E, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.L, Gameboy.MemoryMap.ReadFromMemory(Utils.UshortFromByte(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L)))))},
            {0x6F, new Instruction(1, new Action(() => ld(ref Gameboy.Cpu.Registers.L, Gameboy.Cpu.Registers.A)))},
            {0x70, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x71, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x72, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x73, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x74, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x75, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x76, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x77, new Instruction(8, new Action(() => ld_hl_a()))},
            {0x78, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.B)))},
            {0x79, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.C)))},
            {0x7A, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.D)))},
            {0x7B, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.E)))},
            {0x7C, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.H)))},
            {0x7D, new Instruction(4, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.Cpu.Registers.L)))},
            {0x7E, new Instruction(8, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H,Gameboy.Cpu.Registers.L)))))},
            {0x7F, new Instruction(4, new Action(() => nop()))}, // Moving A into A == nothing lmao, but we still need to spend 4 cycles
            {0x80, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.B)))},
            {0x81, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.C)))},
            {0x82, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.D)))},
            {0x83, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.E)))},
            {0x84, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.H)))},
            {0x85, new Instruction(4, new Action(() => add(Gameboy.Cpu.Registers.L)))},
            {0x86, new Instruction(8, new Action(() => add(Gameboy.MemoryMap.ReadFromMemory(Gameboy.Cpu.GetUshortFromRegisters(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L)))))},
            {0x87, new Instruction(1, new Action(() => add(Gameboy.Cpu.Registers.A)))},
            {0x88, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.B)))},
            {0x89, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.C)))},
            {0x8A, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.D)))},
            {0x8B, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.E)))},
            {0x8C, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.H)))},
            {0x8D, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.L)))},
            {0x8E, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.MemoryMap.ReadFromMemory(Utils.UshortFromByte(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L)))))},
            {0x8F, new Instruction(1, new Action(() => adc(ref Gameboy.Cpu.Registers.A,  Gameboy.Cpu.Registers.A)))},
            {0x90, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x91, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x92, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x93, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x94, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x95, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x96, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x97, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x98, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x99, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9F, new Instruction(4, new Action(() => sbc(Gameboy.Cpu.Registers.A)))},
            {0xA0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA6, new Instruction(8, new Action(() => and(Gameboy.MemoryMap.ReadFromMemory(Utils.UshortFromByte(Gameboy.Cpu.Registers.H, Gameboy.Cpu.Registers.L)))))},
            {0xA7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA8, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.B)))},
            {0xA9, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.C)))},
            {0xAA, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.D)))},
            {0xAB, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.E)))},
            {0xAC, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.H)))},
            {0xAD, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.L)))},
            {0xAE, new Instruction(1, new Action(() =>throw new Exception("Not implemented")))},
            {0xAF, new Instruction(4, new Action(() => xor(Gameboy.Cpu.Registers.A)))},
            {0xB0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC1, new Instruction(1, new Action(() => pop(ref Gameboy.Cpu.Registers.B, ref Gameboy.Cpu.Registers.C)))},
            {0xC2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC3, new Instruction(12, new Action(() => jp()))},
            {0xC4, new Instruction(1, new Action(() => call(!Gameboy.Cpu.Registers.F.Z)))},
            {0xC5, new Instruction(16, new Action(() => push(Gameboy.Cpu.Registers.B, Gameboy.Cpu.Registers.C)))},
            {0xC6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC9, new Instruction(8, new Action(() => ret()))},
            {0xCA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCC, new Instruction(12, new Action(() => call(Gameboy.Cpu.Registers.F.Z)))},
            {0xCD, new Instruction(12, new Action(() => call(true)))},
            {0xCE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD4, new Instruction(12, new Action(() => call(!Gameboy.Cpu.Registers.F.C)))},
            {0xD5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDC, new Instruction(12, new Action(() => call(Gameboy.Cpu.Registers.F.C)))},
            {0xDD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE0, new Instruction(12, new Action(() => ldh(GetFollowingByte(), Gameboy.Cpu.Registers.A)))},
            {0xE1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE2, new Instruction(8, new Action(() => ld_address_r((ushort)(Gameboy.Cpu.Registers.C | 0xFF00), Gameboy.Cpu.Registers.A)))}, // TODO CHECK THIS IMPLEMENTATION
            {0xE3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEA, new Instruction(16, new Action(() => ld_mem(GetFollowingWord(), Gameboy.Cpu.Registers.A)))},
            {0xEB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xED, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF0, new Instruction(12, new Action(() => ld(ref Gameboy.Cpu.Registers.A, Gameboy.MemoryMap.ReadFromMemory((ushort)(0xFF00 | GetFollowingByte())))))},
            {0xF1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF5, new Instruction(16, new Action(() => push(Gameboy.Cpu.Registers.A, Gameboy.Cpu.GetFValue())))},
            {0xF6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF9, new Instruction(8, new Action(() => ld_sp_hl()))},
            {0xFA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFB, new Instruction(4, new Action(() => ei()))},
            {0xFC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFE, new Instruction(1, new Action(() => cp(GetFollowingByte())))},
            {0xFF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
        };

        public static Dictionary<ushort, Instruction> CBInstructionSet = new Dictionary<ushort, Instruction>()
        {
            {0x01, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x02, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x03, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x04, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x05, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x06, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x07, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x08, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x09, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x0F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x10, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.B)))},
            {0x11, new Instruction(8, new Action(() => rl(ref Gameboy.Cpu.Registers.C)))},
            {0x12, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.D)))},
            {0x13, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.E)))},
            {0x14, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.H)))},
            {0x15, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.L)))},
            {0x16, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x17, new Instruction(1, new Action(() => rl(ref Gameboy.Cpu.Registers.A)))},
            {0x18, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x19, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            //{0x1A, new Instruction(8, new Action(() => ld_n_n(ref Gameboy.Cpu.Registers.A, )))},
            {0x1B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x1F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x20, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x21, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x22, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x23, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x24, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x25, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x26, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x27, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x28, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x29, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x2F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x30, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x31, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x32, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x33, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x34, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x35, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x36, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x37, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x38, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x39, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x3F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x40, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x41, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x42, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x43, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x44, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x45, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x46, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x47, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x48, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x49, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x4F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x50, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x51, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x52, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x53, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x54, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x55, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x56, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x57, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x58, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x59, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x5F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x60, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x61, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x62, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x63, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x64, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x65, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x66, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x67, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x68, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x69, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x6F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x70, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x71, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x72, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x73, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x74, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x75, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x76, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x77, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x78, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x79, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x7A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x7B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x7C, new Instruction(8, new Action(() => bit(7, Gameboy.Cpu.Registers.H)))},
            {0x7D, new Instruction(1, new Action(() => bit(7, Gameboy.Cpu.Registers.L)))},
            {0x7E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x7F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x80, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x81, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x82, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x83, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x84, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x85, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x86, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x87, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x88, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x89, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x8F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x90, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x91, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x92, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x93, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x94, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x95, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x96, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x97, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x98, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x99, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9A, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9B, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9C, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9D, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9E, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0x9F, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xA9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xAF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xB9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xBF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xC9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xCF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xD9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xDF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xE9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xED, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xEF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF0, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF1, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF2, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF3, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF4, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF5, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF6, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF7, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF8, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xF9, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFA, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFB, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFC, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFD, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFE, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
            {0xFF, new Instruction(1, new Action(() => throw new Exception("Not implemented")))},
        };
    }
}
