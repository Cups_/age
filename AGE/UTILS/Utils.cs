﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGE.UTILS
{
    public static class Utils
    {
        #region BITWISE_OP
        /// <summary>
        /// Returns ushort from two byte.
        /// </summary>
        /// <param name="b1">High Endian Byte</param>
        /// <param name="b2">Low Endian Byte</param>
        /// <returns></returns>
        public static ushort UshortFromByte(byte b1, byte b2)
        {
            return (ushort)(b1 << 8 | b2);
        }

        /// <summary>
        /// Returns bytes from an ushort.
        /// </summary>
        /// <param name="s1">Return Array [0] Is low endian, [1]High.</param>
        /// <returns></returns>
        public static byte[] BytesFromUShort(ushort s1)
        {
            byte[] returnArray = new byte[2];

            returnArray[0] = (byte)s1;
            returnArray[1] = (byte)(s1 >> 8);

            return returnArray;
        }
        #endregion

        /// <summary>
        /// Convert an interrupt flag to a byte representation.
        /// </summary>
        /// <param name="intF">The interrupt flag to convert.</param>
        /// <returns>The resulting byte.</returns>
        public static byte ByteFromInterrupt(IO.InterruptFlags intF)
        {
            byte returnByte = 0b0000_0000;

            returnByte |= Convert.ToByte(intF.VBlank);
            returnByte |= (byte)(Convert.ToByte(intF.LCDC) << 1);
            returnByte |= (byte)(Convert.ToByte(intF.TimerOverflow) << 2);
            returnByte |= (byte)(Convert.ToByte(intF.SIOTransferComplete) << 3);
            returnByte |= (byte)(Convert.ToByte(intF.TransitionHtL) << 4);

            return returnByte;
        }

        /// <summary>
        /// Convert a byte to an interrupt flag structure.
        /// </summary>
        /// <param name="b">The byte to convert.</param>
        /// <returns>The interrupt flag structure equivalent.</returns>
        public static IO.InterruptFlags InterruptFromByte(byte b)
        {
            IO.InterruptFlags returnFlags = new IO.InterruptFlags();

            returnFlags.VBlank = Convert.ToBoolean(b & 1);
            returnFlags.LCDC = Convert.ToBoolean((b & 2) >> 1);
            returnFlags.TimerOverflow = Convert.ToBoolean((b & 4) >> 2);
            returnFlags.SIOTransferComplete = Convert.ToBoolean((b & 8) >> 3);
            returnFlags.TransitionHtL = Convert.ToBoolean((b & 16) >> 4);

            return returnFlags;
        }
    }
}
