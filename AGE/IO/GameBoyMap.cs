﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using AGE.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static AGE.Shared;

namespace AGE.Map
{
    public class GameBoyMap
    {
        #region MEMORY_BOUNDS
        public static Tuple<int, int> ROM = new Tuple<int, int>(0x0, 0x7FFF);
        public static Tuple<int, int> VRAM = new Tuple<int, int>(0x8000, 0x9FFF);
        public static Tuple<int, int> SRAM = new Tuple<int, int>(0xA000, 0xBFFF);
        public static Tuple<int, int> IRAM = new Tuple<int, int>(0xC000, 0xDFFF);
        public static Tuple<int, int> IRAM_ECHO = new Tuple<int, int>(0xE000, 0xFDFF);       
        public static Tuple<int, int> SAM = new Tuple<int, int>(0xFE00, 0xFE9F);
        public static Tuple<int, int> IO = new Tuple<int, int>(0xFF00, 0xFF4B);
        public static Tuple<int, int> ZPRAM = new Tuple<int, int>(0xFF80, 0xFFFE);
        #endregion

        #region SPECIAL_MEMORIES
        // CPU
        public const int UNMAP_BOOTROM = 0xFF50;
        public const int IF = 0xFF0F;

        // SPU
        public const int NR52 = 0xFF26;
        public const int NR51 = 0xFF25;
        public const int NR11 = 0xFF11;
        public const int NR10 = 0xFF10;

        // GPU
        public const int VBLANK_INTER = 0x0040;
        public const int LCDC_INTER = 0x0048;
        public const int TIMER_OVERFLOW_INTER = 0x0050;

        public const int BGP = 0xFF47;
        public const int SCY = 0xFF42;
        public const int LCDC = 0xFF40;
        public const int LY = 0xFF44;
        #endregion

        public GameBoyIO GameBoyIO = null;

        public bool BootRomMounted = false;
        
        public GameBoyMap()
        {
            this.GameBoyIO = new GameBoyIO();
        }

        private bool InRange(Tuple<int, int> range, int value)
        {
            return ((value >= range.Item1) && (value <= range.Item2));
        }

        public byte ReadFromMemory(ushort address)
        {
            if(InRange(ROM, address))
            {
                return Gameboy.Cartridge.CartridgeRom[address - ROM.Item1];
            }
            if(InRange(SRAM, address))
            {
                return Gameboy.Cartridge.CartridgeRam[address - SRAM.Item1];
            }
            if(InRange(IRAM, address))
            {
                return Gameboy.InternalRAM[address - IRAM.Item1];
            }
            if(InRange(ZPRAM, address))
            {
                return Gameboy.ZeroPageRAM[address - ZPRAM.Item1];
            }
            if(InRange(IRAM_ECHO, address))
            {
                return Gameboy.InternalRAM[address - IRAM_ECHO.Item1];
            }
            if(InRange(SAM, address))
            {
                return Gameboy.Gpu.SpriteAttribMemory[address - SAM.Item1];
            }
            if(InRange(IO, address))
            {
                return GameBoyIO.ReadIO(address);
            }
            
            throw new Exception("Cannot read value. Address is not in any implemented range : " + address);
        }

        public void WriteToMemory(ushort address, byte value)
        {
            if (InRange(ROM, address))
            {
                Gameboy.Cartridge.CartridgeRom[address - ROM.Item1] = value;
            }
            else if (InRange(SRAM, address))
            {
                Gameboy.Cartridge.CartridgeRam[address - SRAM.Item1] = value;
            }
            else if (InRange(IRAM_ECHO, address))
            {
                Gameboy.InternalRAM[address - IRAM_ECHO.Item1] = value;
            }
            else if(InRange(IRAM, address))
            {
                Gameboy.InternalRAM[address - IRAM.Item1] = value;
            }
            else if (InRange(IO, address))
            {
                GameBoyIO.WriteIO(address, value);
            }
            else if (InRange(SAM, address))
            {
                Gameboy.Gpu.SpriteAttribMemory[address - SAM.Item1] = value;
            }
            else if(InRange(VRAM, address))
            {
                Gameboy.Gpu.VRAM[address - VRAM.Item1] = value;
            }
            else if(InRange(ZPRAM, address))
            {
                Gameboy.ZeroPageRAM[address - ZPRAM.Item1] = value;
            }
            else if(address == UNMAP_BOOTROM)
            {
                BootRomMounted = false;
            }
            else
            {
                // This memory location is unknown, is the cartridge trying to write on the fucking moon, or did my lazy ass haven't implemented it yet ? 
                throw new Exception("Cannot write value. Address is not in any implemented range : " + address);
            }        
        }

        public void WriteToMemory(ushort address, ushort value)
        {
            WriteToMemory(address, (byte)(value & 255));
            WriteToMemory((ushort)(address + 1), (byte)(value >> 8));
        }
    }
}
