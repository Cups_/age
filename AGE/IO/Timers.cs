﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGE.IO
{
    public class TAC
    {
        bool TimerEnabled; // bit 2
        byte InputClock; // bit 0 + 1

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= InputClock;
            returnByte |= (byte)(Convert.ToByte(TimerEnabled) << 2);

            return returnByte;
        }
    }

    public class Timers
    {
        /// <summary>
        /// When the TIMA overflows, this data will be loaded. 
        /// </summary>
        public int TimerModulo = 10; // Note : Can take values specified on page 39.

        /// <summary>
        /// TIMA.
        /// </summary>
        public byte TimerCounter = 0x0;

        /// <summary>
        /// True if timer is enabled.
        /// </summary>
        public bool TimerEnabled = true;

        /// <summary>
        /// This register is incremented 16384 (~16779 on SGB) times a second. Writing any value sets it to $00.
        /// </summary>
        public uint DividerRegister = 0;

        /// <summary>
        /// Should we interrupt ? 
        /// </summary>
        public bool Interrupt = false;

        public void ConfirmInterrupt()
        {
            // The CPU got out interrupt.
            Interrupt = false;
        }

        public void Tick()
        {
            // This register is always incremented.
            DividerRegister++;

            if (!TimerEnabled) return;

            if(DividerRegister % 16384 == 0)
            {
                // Divider Ticked.
                if(TimerCounter + 1 > 255)
                {
                    TimerCounter = (byte)TimerModulo;
                    Interrupt = true;
                }
                else
                {
                    TimerCounter++;
                }
            }
        }
    }
}
