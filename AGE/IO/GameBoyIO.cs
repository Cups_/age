﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGE.UTILS;
using static AGE.Shared;
namespace AGE.IO
{
    public struct InterruptFlags
    {
        // Highest priority.
        public bool VBlank;
        public bool LCDC;
        public bool TimerOverflow;
        public bool SIOTransferComplete;
        public bool TransitionHtL;
        // Lowest priority.

        public InterruptFlags(int i = 0)
        {
            VBlank = false;
            LCDC = false;
            TimerOverflow = false;
            SIOTransferComplete = false;
            TransitionHtL = false;
        }
    }

    public class GameBoyIO
    {
        public InterruptFlags Interrupt;
     
        
        public GameBoyIO()
        {
            // Init interrupt flags
            Interrupt = new InterruptFlags();
        }

        public void WriteIO(ushort gloAddress, byte value)
        {
            switch (gloAddress)
            {
                case Map.GameBoyMap.IF:
                    Console.WriteLine("Set new Interrupt :");
                    Interrupt = Utils.InterruptFromByte(value);
                    break;
                case Map.GameBoyMap.NR52:
                    Gameboy.Spu.NR52 = SPU.NR52.ToNR52(value);
                    Console.WriteLine("Set new NR52.");
                    break;
                case Map.GameBoyMap.NR11:
                    Gameboy.Spu.NR11 = SPU.NR11.ToNR11(value);
                    Console.WriteLine("Set new NR11.");
                    break;
                case Map.GameBoyMap.NR10:
                    Gameboy.Spu.NR10 = SPU.NR10.ToNR10(value);
                    Console.WriteLine("Set new NR10.");
                    break;
                case Map.GameBoyMap.NR51:
                    Gameboy.Spu.NR51 = SPU.NR51.ToNR51(value);
                    Console.WriteLine("Set new NR51.");
                    break;
                case Map.GameBoyMap.BGP:
                    Gameboy.Gpu.BGP = GPU.BGP.ToBGP(value);
                    Console.WriteLine("Set new BG & Window Palette.");
                    break;
                case Map.GameBoyMap.SCY:
                    Gameboy.Gpu.ScrollY = value;
                    break;
                case Map.GameBoyMap.LCDC:
                    Gameboy.Gpu.LCDControl = GPU.LCDC.ToLCDC(value);
                    Console.WriteLine("Set new LCDC.");
                    break;
                default:
                    throw new Exception("Trying to write to an unimplemented IO address : 0x" + gloAddress.ToString("X"));
            }
        }

        public byte ReadIO(ushort gloAddress)
        {
            switch (gloAddress)
            {
                case Map.GameBoyMap.IF:
                    Console.WriteLine("Trying to read interrupt value.");
                    return UTILS.Utils.ByteFromInterrupt(Interrupt);
                case Map.GameBoyMap.LY:
                    Console.WriteLine("Trying to read VBlank flag");
                    return Gameboy.Gpu.LY;
                default:
                    throw new Exception("Trying to read from an unimplemented IO address : 0x" + gloAddress.ToString("X"));
            }
        }
    }
}
