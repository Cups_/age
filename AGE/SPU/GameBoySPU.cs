﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGE.SPU
{
    public class NR52
    {
        // Bit 7
        public bool SoundEnabled { get; set; } = false;

        // Bit 3
        public bool Sound4Enabled { get; set; } = false;
        public bool Sound3Enabled { get; set; } = false;
        public bool Sound2Enabled { get; set; } = false;
        public bool Sound1Enabled { get; set; } = false;
        // Bit 0

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= Convert.ToByte(Sound1Enabled);
            returnByte |= (byte)(Convert.ToByte(Sound2Enabled) << 1);
            returnByte |= (byte)(Convert.ToByte(Sound3Enabled) << 2);
            returnByte |= (byte)(Convert.ToByte(Sound4Enabled) << 3);
            returnByte |= (byte)(Convert.ToByte(SoundEnabled) << 7);

            return returnByte;
        }

        public static NR52 ToNR52(byte b)
        {
            NR52 returnNR52 = new NR52();

            returnNR52.Sound1Enabled = Convert.ToBoolean(b & 1);
            returnNR52.Sound2Enabled = Convert.ToBoolean((b & 2) >> 1);
            returnNR52.Sound3Enabled = Convert.ToBoolean((b & 4) >> 2);
            returnNR52.Sound4Enabled = Convert.ToBoolean((b & 8) >> 3);

            returnNR52.SoundEnabled = Convert.ToBoolean((b & 128) >> 7);

            return returnNR52;
        }
    }

    public class NR11
    {
        // Bit 7-6
        public byte WavePatternDuty { get; set; } = 0;

        // Bit 5-0
        public byte SoundLengthData { get; set; } = 0;

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= (byte)(WavePatternDuty << 6);
            returnByte |= (byte)(SoundLengthData);

            return returnByte;
        }

        public static NR11 ToNR11(byte b)
        {
            NR11 returnNR11 = new NR11();

            returnNR11.SoundLengthData = (byte)(b & 63);
            returnNR11.WavePatternDuty = (byte)((b & 196) >> 6);

            return returnNR11;
        }
    }   

    public class NR10
    {
        public byte SweepTime { get; set; } = 0;
        public bool SweepIncreaseDecrease { get; set; } = false;
        public byte SweepShift { get; set; } = 0;

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= (byte)(SweepShift);
            returnByte |= (byte)(Convert.ToByte(SweepIncreaseDecrease) << 3);
            returnByte |= (byte)(SweepTime << 4);

            return returnByte;
        }

        public static NR10 ToNR10(byte b)
        {
            NR10 returnNR10 = new NR10();

            returnNR10.SweepTime = (byte)((b & 112) >> 4);
            returnNR10.SweepIncreaseDecrease = Convert.ToBoolean((b & 8) >> 3);
            returnNR10.SweepShift = (byte)((b & 7));

            return returnNR10;
        }
    }
    
    public class NR51
    {
        public bool Sound4SO2 { get; set; } = false;
        public bool Sound3SO2 { get; set; } = false;
        public bool Sound2SO2 { get; set; } = false;
        public bool Sound1SO2 { get; set; } = false;
        public bool Sound4SO1 { get; set; } = false;
        public bool Sound3SO1 { get; set; } = false;
        public bool Sound2SO1 { get; set; } = false;
        public bool Sound1SO1 { get; set; } = false;

        public byte ToByte()
        {
            byte returnByte = 0b0000_0000;

            returnByte |= (byte)(Convert.ToByte(Sound1SO1));
            returnByte |= (byte)(Convert.ToByte(Sound2SO1) << 1);
            returnByte |= (byte)(Convert.ToByte(Sound3SO1) << 2);
            returnByte |= (byte)(Convert.ToByte(Sound4SO1) << 3);
            returnByte |= (byte)(Convert.ToByte(Sound1SO2) << 4);
            returnByte |= (byte)(Convert.ToByte(Sound2SO2) << 5);
            returnByte |= (byte)(Convert.ToByte(Sound3SO2) << 6);
            returnByte |= (byte)(Convert.ToByte(Sound4SO2) << 7);

            return returnByte;
        }

        public static NR51 ToNR51(byte b)
        {
            NR51 returnNR51 = new NR51();

            returnNR51.Sound1SO1 = Convert.ToBoolean(b & 1);
            returnNR51.Sound2SO1 = Convert.ToBoolean((b & 2) >> 1);
            returnNR51.Sound3SO1 = Convert.ToBoolean((b & 4) >> 2);
            returnNR51.Sound4SO1 = Convert.ToBoolean((b & 8) >> 3);
            returnNR51.Sound1SO2 = Convert.ToBoolean((b & 16) >> 4);
            returnNR51.Sound2SO2 = Convert.ToBoolean((b & 32) >> 5);
            returnNR51.Sound3SO2 = Convert.ToBoolean((b & 64) >> 6);
            returnNR51.Sound4SO2 = Convert.ToBoolean((b & 128) >> 7);

            return returnNR51;
        }
    }
    
    public class GameBoySPU
    {
        public NR52 NR52 { get; set; }
        public NR51 NR51 { get; set; }
        public NR11 NR11 { get; set; }
        public NR10 NR10 { get; set; }

        public GameBoySPU()
        {
            NR52 = new NR52();
            NR51 = new NR51();
            NR11 = new NR11();
            NR10 = new NR10();
        }
    }
}
