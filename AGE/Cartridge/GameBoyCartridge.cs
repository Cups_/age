﻿/*
    AGE
    Copyright (C) 2019  Cups

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AGE.Cartridge
{

    public class GameBoyCartridge
    {
        /// <summary>
        /// A sequence of bytes 00 C3 xx xx where last two bytes contain the
        /// starting address of a cartridge(lower byte first). The first two
        ///  bytes of this sequence can be used as a "magic number" to
        /// recognize GameBoy cartridges. When GameBoy starts, the control is 
        /// passed to address 0100 and then the sequence is interpreted as    
        /// NOP; JP.
        /// </summary>
        public byte[] FourHeader { get; private set; } = new byte[4];

        /// <summary>
        /// Nintendo character area
        /// </summary>
        public byte[] NintendoCharacterArea { get; private set; } = new byte[0x0133 - 0x0105 + 1];

        /// <summary>
        /// When GameBoy starts, the control is 
        //  passed to address 0100 and then the sequence is interpreted as    
        //  NOP; JP.
        /// </summary>
        public byte[] EntryPoint { get; private set; } = new byte[2];

        /// <summary>
        /// A sequence of bytes 0x00 0xC3 representing Gameboy Cartridge, can be used for recognition
        /// </summary>
        public byte[] MagicNumber { get; private set; } = new byte[2];

        /// <summary>
        /// Do you really need an explanation
        /// </summary>
        public string Title { get; private set; } = "Unknown Title";

        /// <summary>
        /// The Cartidge Type
        /// </summary>
        public CartridgeType Type { get; private set; } = CartridgeType.UNKNOWN;

        /// <summary>
        /// The ROM Size in kBits, divide by 16 for bank amount
        /// </summary>
        public uint RomSize { get; private set; } = 0;

        /// <summary>
        /// The RAM Size, divide by 16 for bank amount
        /// </summary>
        public uint RamSize { get; private set; } = 0;

        /// <summary>
        /// The Cartridge Language
        /// </summary>
        public CartridgeLanguage CartridgeLanguage { get; private set; } = CartridgeLanguage.ENGLISH;

        /// <summary>
        /// The CartridgeVersion
        /// </summary>
        public uint Version { get; private set; } = 0;

        /// <summary>
        /// Checksum (higher byte first) produced by adding all bytes of   
        //  a cartridge except for two checksum bytes together and taking
        ///   two lower bytes of the result.
        /// </summary>
        public byte[] Checksum { get; private set; } = new byte[0x014F - 0x014E + 1];

        /// <summary>
        /// Contains the full byte array that the cartridge contains
        /// </summary>
        public byte[] CartridgeRom = null;

        /// <summary>
        /// Emulation of the cartridge RAM
        /// </summary>
        public byte[] CartridgeRam = null;

        public GameBoyCartridge(MemoryStream memstream)
        {
            ReadPropertiesFromMemory(memstream);
            CartridgeRom = memstream.GetBuffer();
            CartridgeRam = new byte[128*1024]; // We emulate a RAM size of 128kB, it being the maximum RAM we could find.
            memstream.Close();
        }

        private void ReadPropertiesFromMemory(MemoryStream mem)
        {
            int singleByte = 0;
            byte[] memBuffer = mem.GetBuffer();

            if (!mem.CanRead)
                throw new Exception("Stream cannot be read.");

            // Read 0x0100-0x0103
            mem.Seek(0x0100, SeekOrigin.Begin);
            mem.Read(FourHeader, 0, 4);

            this.EntryPoint[0] = FourHeader[3];
            this.EntryPoint[1] = FourHeader[2];

            this.MagicNumber[0] = FourHeader[0];
            this.MagicNumber[1] = FourHeader[1];

            // Read 0x0105-0x0133
            mem.Seek(0x015, SeekOrigin.Begin);
            mem.Read(NintendoCharacterArea, 0, 0x0133 - 0x0105 + 1);

            // Read 0x0134-0x0143
            byte[] ASCIITitle = new byte[0x0143 - 0x0134 + 1];
            mem.Seek(0x0134, SeekOrigin.Begin);
            mem.Read(ASCIITitle, 0, ASCIITitle.Length);
            this.Title = Encoding.ASCII.GetString(ASCIITitle);

            /* Read 0147
            mem.Seek(0x0147, SeekOrigin.Begin);
            singleByte = mem.ReadByte();
            this.Type = (CartridgeType) Enum.GetValues(typeof(CartridgeType)).GetValue(b);
            */

            // Read 0x0148
            mem.Seek(0x0148, SeekOrigin.Begin);
            singleByte = mem.ReadByte();
            switch (singleByte)
            {
                case 0:
                    this.RomSize = 256;
                    break;
                case 1:
                    this.RomSize = 512;
                    break;
                case 2:
                    this.RomSize = 1024;
                    break;
                case 3:
                    this.RomSize = 2048;
                    break;
                case 4:
                    this.RomSize = 4096;
                    break;
                case 5:
                    this.RomSize = 8192;
                    break;
                case 6:
                    this.RomSize = 16384;
                    break;
                default:
                    // Couldn't get ROM Size, throw error or try to guess ? 
                    this.RomSize = 4096;
                    break;                
            }

            // Read 0x0149   
            mem.Seek(0x0149, SeekOrigin.Begin);
            singleByte = mem.ReadByte();
            switch (singleByte)
            {
                case 0:
                    this.RamSize = 0;
                    break;
                case 1:
                    this.RamSize = 16;
                    break;
                case 2:
                    this.RamSize = 64;
                    break;
                case 3:
                    this.RamSize = 256;
                    break;
                case 4:
                    this.RamSize = 512;
                    break;
                case 5:
                    this.RamSize = 1024;
                    break;
                case 6:
                    this.RamSize = 2048;
                    break;
                default:
                    // Couldn't get RAM Size, throw error or try to guess ? 
                    this.RamSize = 256;
                    break;
            }


            // Read 0x014A       
            mem.Seek(0x014A, SeekOrigin.Begin);
            singleByte = mem.ReadByte();
            this.CartridgeLanguage = (CartridgeLanguage)Enum.GetValues(typeof(CartridgeLanguage)).GetValue(singleByte);
        }

    }

    public enum CartridgeManufacturerCode
    {
        NINTENDO_OR_EXTENDED = 0x33,
        ACCOLADE = 0x79, 
        KONAMI = 0xA4
    }

    public enum CartridgeLanguage
    {
        JAPANESE = 0,
        ENGLISH = 1
    }

    public enum CartridgeType
    {
        ROM_ONLY = 0,
        ROM_MBC1 = 1,
        ROM_MBC1_RAM = 2,
        ROM_MBC1_RAM_BATTERY = 3,
        UNKNOWN = 4,
        ROM_MBC2 = 5,
        ROM_MBC2_BATTERY = 6
    }
}
