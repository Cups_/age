﻿namespace AGE
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCartridgeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameboyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.togglePowerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ScreenPanel = new System.Windows.Forms.Panel();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugButtonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.gameboyToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(640, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCartridgeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadCartridgeToolStripMenuItem
            // 
            this.loadCartridgeToolStripMenuItem.Name = "loadCartridgeToolStripMenuItem";
            this.loadCartridgeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadCartridgeToolStripMenuItem.Text = "Load Cartridge";
            this.loadCartridgeToolStripMenuItem.Click += new System.EventHandler(this.loadCartridgeToolStripMenuItem_Click);
            // 
            // gameboyToolStripMenuItem
            // 
            this.gameboyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.togglePowerToolStripMenuItem});
            this.gameboyToolStripMenuItem.Name = "gameboyToolStripMenuItem";
            this.gameboyToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.gameboyToolStripMenuItem.Text = "Gameboy";
            // 
            // togglePowerToolStripMenuItem
            // 
            this.togglePowerToolStripMenuItem.Name = "togglePowerToolStripMenuItem";
            this.togglePowerToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.togglePowerToolStripMenuItem.Text = "Toggle Power";
            this.togglePowerToolStripMenuItem.Click += new System.EventHandler(this.togglePowerToolStripMenuItem_Click);
            // 
            // ScreenPanel
            // 
            this.ScreenPanel.BackColor = System.Drawing.Color.White;
            this.ScreenPanel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ScreenPanel.Location = new System.Drawing.Point(0, 24);
            this.ScreenPanel.Name = "ScreenPanel";
            this.ScreenPanel.Size = new System.Drawing.Size(640, 576);
            this.ScreenPanel.TabIndex = 1;
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugButtonToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // debugButtonToolStripMenuItem
            // 
            this.debugButtonToolStripMenuItem.Name = "debugButtonToolStripMenuItem";
            this.debugButtonToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.debugButtonToolStripMenuItem.Text = "Debug Button";
            this.debugButtonToolStripMenuItem.Click += new System.EventHandler(this.debugButtonToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(640, 600);
            this.Controls.Add(this.ScreenPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "AGE - Gameboy Emulator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCartridgeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gameboyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem togglePowerToolStripMenuItem;
        private System.Windows.Forms.Panel ScreenPanel;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugButtonToolStripMenuItem;
    }
}

